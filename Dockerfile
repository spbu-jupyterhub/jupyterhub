FROM python:3.6.3-alpine3.6

ARG CI_JOB_TOKEN

RUN \
	apk --update add --virtual .build-dependencies git gcc musl-dev python3-dev libffi-dev openssl-dev && \
	pip3 install --no-cache \
		cryptography \
		jupyterhub==0.9.1 \
		git+https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/spbu-jupyterhub/luhepauth.git@0.9.0 \
	&& \
	apk del .build-dependencies && \
	apk add openssl && \
	rm /var/cache/apk/* && \
	(find / -depth \(    \( -type d -a \( -name test -o -name tests \) \)    -o    \( -type f -a \( -name '*.pyc' -o -name '*.pyo' \) \)   \) -exec rm -rf '{}' + 2>/dev/null || true)

ADD https://raw.githubusercontent.com/jupyterhub/jupyterhub/0.9.1/examples/cull-idle/cull_idle_servers.py /cull_idle_servers.py

ENV LANG=en_US.UTF-8

WORKDIR /srv/jupyterhub/

CMD ["jupyterhub"]
